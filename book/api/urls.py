from django.conf.urls import url
from .views import CardListView, CardView
from book.api.views import BookView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^cards/id/(?P<card_id>[0-9+])/$', CardView.as_view()),
    url(r'^cards/$', CardListView.as_view(),name='main_book'),
    url(r'^book/$', BookView,name='main_book'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

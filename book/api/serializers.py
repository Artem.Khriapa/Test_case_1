from rest_framework import serializers
from ..models import Card, PhoneNumber
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User



class ThinCardSerializer(serializers.ModelSerializer):
    class Meta:
                model = Card
                fields =(
                    'id',
                    'surname',
                )

class PhoneSerializer(serializers.ModelSerializer):
    related_cards = ThinCardSerializer(read_only=True, many=True)

    class Meta:
        model = PhoneNumber
        fields = (
            # 'id',
            'number',
            'related_cards'
        )

class CardSerializer(serializers.ModelSerializer):

    phone = PhoneSerializer(read_only=True, many=True)
    phonenumber = serializers.CharField(write_only = True)
    email = serializers.CharField(
        max_length=100,
        validators=[UniqueValidator(queryset=Card.objects.all())]
    )
    class Meta:
            model = Card
            fields =(
                'id',
                'name',
                'surname',
                'location',
                'email',
                'phone',
                'phonenumber',
                'user'
            )

    def validate(self, data):
        for phone in data.get('phonenumber').split(","):
            if not data.get('phonenumber'):
                raise serializers.ValidationError("Please enter the phone number.")
            if len(data.get('phonenumber')) < 13 or data.get('phonenumber')[0] != '+' \
                    or data.get('phonenumber')[:1].isdigit():
                raise serializers.ValidationError("Please enter the phone number in international format : +XXXXXXXXXXXX .")
            return data

    def create(self, validated_data):
        card = Card.objects.create(
            name = validated_data['name'],
            surname = validated_data['surname'],
            email = validated_data['email'],
            location = validated_data['location'],
            user =  self.context['request'].user
        )
        card.save()
        for numb in validated_data['phonenumber'].split(","):
            phone = PhoneNumber.objects.create(number=numb)
            card.phone.add(phone)
        card.save()
        return card


from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from utils.helpers_for_tests import dump
from book.models import Card, PhoneNumber
from utils.helpers_for_tests import create_user, login_user
class BookTest(TestCase):

    def setUp(self):
        self.user = create_user('SomeTestUser')
        self.c = APIClient()
        self.c.login(username='SomeTestUser', password='1111')
        self.p_numb = PhoneNumber.objects.create(number = '+380633333333')
        self.p_numb.save()
        self.card = Card.objects.create(
            name ='test_name',
            surname = 'test_surname',
            location = 'Kyiv oblast',
            email = 'test@test.mail',
            user = self.user
        )
        self.card.save()
        self.card.phone.add(self.p_numb)
        self.card.save()

    def test_get_all_cards(self):
        response = self.c.get('/api/cards/')
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)

    def test_get_cards_without_login(self):
        self.c.logout()
        response = self.c.get('/api/cards/')
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_get_card(self):
        response = self.c.get('/api/cards/id/{}/'.format(self.card.id))
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], self.card.name)

    def test_post_card_validation_wrong_data(self):
        response = self.c.post('/api/cards/',
                data={

                    'name': 'some_name',
                    'surname': 'some_name',
                    'location': 'Kyiv oblast',
                    'email': 'test@test.mail',
                    'phone' : self.p_numb,
                    'phonenumber' : '+380677777777',
                    'user' :  int(self.user.id)
                }
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['email'], ['This field must be unique.'])

        response = self.c.post('/api/cards/',
                data={

                    'name': '',
                    'surname': 'some_name',
                    'location': 'Kyiv oblast',
                    'email': 'test@test.mail_new',
                    'phone' : self.p_numb,
                    'phonenumber' : '+380677777777',
                    'user' :  int(self.user.id)
                }
        )
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['name'], ['This field may not be blank.'])

        response = self.c.post('/api/cards/',
                data={

                    'name': 'some_name',
                    'surname': '',
                    'location': 'Kyiv oblast',
                    'email': 'test@test.mail_new',
                    'phone' : self.p_numb,
                    'phonenumber' : '+380677777777',
                    'user' :  int(self.user.id)
                }
        )
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['surname'], ['This field may not be blank.'])

        response = self.c.post('/api/cards/',
                data={

                    'name': 'some_name',
                    'surname': 'some_surname',
                    'location': 'Kyiv oblast',
                    'email': '',
                    'phone' : self.p_numb,
                    'phonenumber' : '+380677777777',
                    'user' :  int(self.user.id)
                }
        )
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['email'], ['This field may not be blank.'])

        response = self.c.post(
                '/api/cards/',
                data={

                    'name': 'some_name',
                    'surname': 'some_surname',
                    'location': 'Kyiv oblast',
                    'email': 'test@test.mail_new',
                    'phone' : self.p_numb,
                    'phonenumber' : '5555555',
                    'user' : int(self.user.id)
                }
        )
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['non_field_errors'], ['Please enter the phone number in international format : +XXXXXXXXXXXX .'])

    def test_patch_card(self):
        response = self.c.patch(
            '/api/cards/id/{}/'.format(self.card.id),
            data={

                'name': 'some_name',
                'surname': 'some_surname',
                'location': 'Kyiv oblast',
                'email': 'test@test.mail_new',
                'phone' : self.p_numb,
                'phonenumber' : '+380635555555',
                }
            )
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'some_name')

    def test_create_and_delete_card(self):
        self.newp_numb = PhoneNumber.objects.create(number = '+38062222222')
        self.newp_numb.save()

        self.newcard = Card.objects.create(
            name ='for',
            surname = 'deleting',
            location = 'Kyiv oblast',
            email = 'deletetest@test.mail',
            user =  self.user
        )
        self.newcard.save()
        self.newcard.phone.add(self.newp_numb)
        self.newcard.save()

        response = self.c.get('/api/cards/id/{}/'.format(self.newcard.id))
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], self.newcard.name)
        response = self.c.delete('/api/cards/id/{}/'.format(self.newcard.id))
        # print(dump(response))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data, None)